module codeberg.org/Ast3nix/ultraviolet

go 1.21.6

require (
	github.com/jwalton/gchalk v1.3.0
	github.com/k0kubun/pp/v3 v3.2.0
)

require (
	github.com/jwalton/go-supportscolor v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/term v0.16.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
