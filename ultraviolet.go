// Package ultraviolet provides the main logging interface for Ultraviolet.
// Ultraviolet supports two environment variables — LOG_LEVELS and LOG.
//
// LOG (separated by commas, supports wildcards) controls which log scopes are enabled.
// Subscopes are separated by the ScopeSeparator set in the logger (it's janky)
//
// LOG_LEVELS (separated by commas) controls which log levels are enabled.
// The default levels are debug, info, warn, error, critical, all of which are on by default.
package ultraviolet

import (
	"fmt"
	"os"
	"path"
	"runtime/debug"
	"strings"
	"sync/atomic"
	"time"

	clk "github.com/jwalton/gchalk"
	"github.com/k0kubun/pp/v3"

	"io"
)

func setScopesVarPresent() bool {
	_, exists := os.LookupEnv("LOG")
	return exists
}

func setVarScopes() []string {
	value := os.Getenv("LOG")
	return strings.Split(value, ",")
}

func setLogLevelsVarPresent() bool {
	_, exists := os.LookupEnv("LOG_LEVELS")
	return exists
}

func setVarLogLevels() []string {
	value := os.Getenv("LOG_LEVELS")
	return strings.Split(value, ",")
}

var scopesVarPresent = setScopesVarPresent()
var logLevelsVarPresent = setLogLevelsVarPresent()

var enabledScopes = setVarScopes()
var varLogLevels = setVarLogLevels()

var lGray = clk.Ansi256(250)

// DefaultPrefixes is a map of the default prefixes in the library
var DefaultPrefixes = map[string]string{
	"debug":    clk.Gray("%s") + "(" + clk.Gray("?") + "):",
	"info":     clk.BrightBlue("%s") + "(" + clk.BrightBlue("i") + "):",
	"warn":     clk.WithBrightYellow().Bold("%s") + "(" + clk.WithBrightYellow().Bold("#") + "):",
	"error":    clk.WithRed().Bold("%s") + "(" + clk.WithRed().Bold("!") + "):",
	"critical": clk.WithBrightMagenta().Bold("%s") + "(" + clk.WithBrightMagenta().Bold("CRITICAL") + "):",
}

// Logger is the main logger structure. It can be cloned using uv.Clone()
// Note: "format" refers to the format used in Printf()
type Logger struct {
	// Whether the logger will print anything
	Toggled bool

	// The scope of the logger's messages, is inferred from
	// the main module name by default.
	Scope string

	ThreadSafe bool // Makes ratelimiting thread-safe with `atomic` at the cost of performance

	TimestampFormat string // Enables timestamps. Accepts Go's standard timestamp format.
	TimestampIsUTC  bool   // Whether the timestamp should be in UTC

	// Enables the ratelimiting feature.
	RatelimitThroughput uint64 // The amount of messages allowed in a timeframe
	RatelimitInterval   uint64 // The desired timeframe divided by RatelimitThroughput (microseconds)
	ratelimitingBuffer  uint64
	lastMessage         int64

	atomicBuffer      atomic.Uint64
	atomicLastMessage atomic.Int64

	scopeIsMain    bool   // See docs for uv.Subscope()
	ScopeSeparator string // Separator between subscopes

	// Prefixes for different log levels. The default log levels are:
	// debug, info, warn, error, critical
	// Custom log levels can also be added and used via uv.WithLevel()
	Prefixes       map[string]string
	UnsignedFormat string // Format for unsigned integers

	PIntFormat string // Format for positive integers
	NIntFormat string // Format for negative integers

	PFloatFormat string // Format for positive floats
	NFloatFormat string // Format for negative floats

	TrueFormat  string // Format for 'true'
	FalseFormat string // Format for 'false'

	// Log levels that are displayed (default: empty, empty = all levels)
	// The env variable LOG_LEVELS (separated by commas) overrides this
	EnabledLevels []string

	Output io.Writer // Stream to write to, os.Stderr by default

	// This function runs after uv.Critical() or uv.Fatal() is called. Does nothing by default.
	// The arguments are whatever was passed to Critical/Fatal
	ExitFunction func(args ...any)

	// This function is used to format complex objects such as maps and arrays.
	// Uses pkg.go.dev/github.com/k0kubun/pp/v3 by default!
	ObjectFormatter func(obj any) string
}

// SimpleLogger is a simplified version of Logger
// which can be converted using ConvertSimple(),
// check documentation for Logger for more detailed explanations
type SimpleLogger struct {
	Prefixes    map[string]string // Prefixes for different log levels
	scopeIsMain bool              // See docs for uv.Subscope()

	Scope          string // Scope of the logger's messages
	ScopeSeparator string // Separator between subscopes
	FloatFormat    string // Format for floats
	IntegerFormat  string // Format for integers
	BooleanFormat  string // Format for booleans

	ExitFunction func(args ...any)

	Output io.Writer
}

// ConvertSimple turns a SimpleLogger into a Logger.
func ConvertSimple(simple *SimpleLogger) *Logger {
	return &Logger{
		Toggled: true,

		ExitFunction:   simple.ExitFunction,
		FalseFormat:    simple.BooleanFormat,
		NFloatFormat:   simple.FloatFormat,
		NIntFormat:     simple.IntegerFormat,
		Output:         simple.Output,
		PFloatFormat:   simple.FloatFormat,
		PIntFormat:     simple.IntegerFormat,
		Prefixes:       simple.Prefixes,
		Scope:          simple.Scope,
		TrueFormat:     simple.BooleanFormat,
		UnsignedFormat: simple.IntegerFormat,

		ScopeSeparator: simple.ScopeSeparator,
		scopeIsMain:    simple.scopeIsMain,

		EnabledLevels:   []string{"debug", "info", "warn", "error", "critical"},
		ObjectFormatter: func(obj any) string { return pp.Sprint(obj) },
	}
}

// New creates a new logger with the default settings
func New() *Logger {
	buildInfo, ok := debug.ReadBuildInfo()
	scope := ""

	if ok {
		tmp := strings.Split(buildInfo.Main.Path, "/")
		scope = tmp[len(tmp)-1]
	} else {
		scope = "main"
	}

	return &Logger{
		Toggled: true,

		Scope:       scope,
		scopeIsMain: true,

		Prefixes:        DefaultPrefixes,
		TimestampFormat: time.Kitchen,
		TimestampIsUTC:  false,

		UnsignedFormat: clk.WithAnsi256(214).Paint("+%d"),
		PIntFormat:     clk.Cyan("%d"),
		PFloatFormat:   clk.Cyan("%f"),
		NIntFormat:     clk.Blue("%d"),
		NFloatFormat:   clk.Blue("%f"),
		TrueFormat:     clk.Green("%t"),
		FalseFormat:    clk.Red("%t"),

		EnabledLevels: []string{},

		ExitFunction:    func(args ...any) {},
		ObjectFormatter: func(obj any) string { return pp.Sprint(obj) },

		ScopeSeparator: "/",
	}
}

// NewSimple creates a logger with simplified default settings
func NewSimple() *Logger {
	buildInfo, ok := debug.ReadBuildInfo()
	scope := ""
	if ok {
		tmp := strings.Split(buildInfo.Main.Path, "/")
		scope = tmp[len(tmp)-1]
	} else {
		scope = "main"
	}

	return ConvertSimple(&SimpleLogger{
		Scope:        scope,
		scopeIsMain:  true,
		Output:       os.Stderr,
		ExitFunction: func(args ...any) {},
		Prefixes:     DefaultPrefixes,

		FloatFormat:   clk.Cyan("%f"),
		IntegerFormat: clk.Cyan("%d"),
		BooleanFormat: clk.Green("%t"),

		ScopeSeparator: "/",
	})
}

// WithLevel prints a message with a specified log level.
func (uv *Logger) WithLevel(level string, args ...any) {
	if logLevelsVarPresent || len(uv.EnabledLevels) != 0 {
		levels := []string{}
		if logLevelsVarPresent {
			levels = varLogLevels
		} else {
			levels = uv.EnabledLevels
		}

		enabled := false
		for _, lvl := range levels {
			if level == lvl {
				enabled = true
				break
			}
		}

		if !enabled {
			return
		}
	}

	if scopesVarPresent {
		matches := false
		for _, scope := range enabledScopes {
			if scope == "*" {
				matches = true
				break
			}

			matched, err := path.Match(scope, uv.Scope)
			if err != nil {
				uv.Error("Error occured while checking scope pattern", scope, err)
			} else if matched {
				matches = true
				break
			}
		}

		if !matches {
			return
		}
	}

	var toPrint []any
	if uv.TimestampFormat != "" {
		if uv.TimestampIsUTC {
			toPrint = []any{clk.Gray(time.Now().UTC().Format(uv.TimestampFormat)),
				fmt.Sprintf(uv.Prefixes[level], uv.Scope)}
		} else {
			toPrint = []any{clk.Gray(time.Now().Format(uv.TimestampFormat)),
				fmt.Sprintf(uv.Prefixes[level], uv.Scope)}
		}
	}

	for k, v := range args {
		switch v.(type) {
		case uint, uint8, uint16, uint32, uint64:
			if v == 0 {
				args[k] = clk.Bold(fmt.Sprintf(uv.UnsignedFormat, v))
			} else {
				args[k] = fmt.Sprintf(uv.UnsignedFormat, v)
			}
		case int, int8, int16, int32, int64:
			if a := fmt.Sprint(v); a[0] == 45 {
				args[k] = fmt.Sprintf(uv.NIntFormat, v)
			} else if a[0] == 48 {
				args[k] = fmt.Sprintf(clk.Bold(uv.PIntFormat), v)
			} else {
				args[k] = fmt.Sprintf(uv.PIntFormat, v)
			}
		case float32, float64:
			if a := fmt.Sprint(v); a[0] == 45 {
				args[k] = fmt.Sprintf(uv.NFloatFormat, v)
			} else if a[0] == 48 {
				args[k] = fmt.Sprintf(clk.Bold(uv.PFloatFormat), v)
			} else {
				args[k] = fmt.Sprintf(uv.PFloatFormat, v)
			}
		case bool:
			if v == true {
				args[k] = fmt.Sprintf(uv.TrueFormat, v)
			} else {
				args[k] = fmt.Sprintf(uv.FalseFormat, v)
			}
		case string:
			args[k] = v
		default:
			args[k] = pp.Sprint(v)
		}
	}

	toPrint = append(toPrint, args...)

	if uv.RatelimitThroughput != 0 {
		if uv.ThreadSafe {
			if uv.atomicBuffer.Load() == uv.RatelimitThroughput {
				time.Sleep(time.Duration(uv.RatelimitInterval))
			} else if uv.atomicBuffer.Load() != 0 {
				uv.atomicBuffer.Store(uv.atomicBuffer.Load() - uint64(time.Now().UnixMicro()-uv.lastMessage)/
					uv.RatelimitInterval)
			}
			uv.atomicLastMessage.Store(time.Now().UnixMicro())
		} else {
			if uv.ratelimitingBuffer == uv.RatelimitThroughput {
				time.Sleep(time.Duration(uv.RatelimitInterval))
			} else if uv.ratelimitingBuffer != 0 {
				uv.ratelimitingBuffer -= uint64(time.Now().UnixMicro()-uv.lastMessage) /
					uv.RatelimitInterval
			}
			uv.lastMessage = time.Now().UnixMicro()
		}
	}

	fmt.Println(toPrint...)
}

// Clone creates an identical copy of this logger
func (uv *Logger) Clone() *Logger {
	clone := *uv // "go vet" gives a false positive
	clone.Prefixes = uv.Prefixes
	clone.atomicBuffer = atomic.Uint64{}
	clone.atomicLastMessage = atomic.Int64{}

	return &clone
}

// Subscope clones this logger and adds a subscope to it.
// If the logger's previous scope was inferred, it is replaced.
// Otherwise, it is appended using a separator from uv.ScopeSeparator
func (uv *Logger) Subscope(scope string) *Logger {
	clone := uv.Clone()
	if clone.scopeIsMain == true {
		clone.Scope = scope
		clone.scopeIsMain = false
		return clone
	}
	clone.Scope += uv.ScopeSeparator + scope
	return clone
}

// Debug sends a message with the "debug" log level.
func (uv *Logger) Debug(args ...any) {
	uv.WithLevel("debug", args...)
}

// Info sends a message with the "info" log level.
func (uv *Logger) Info(args ...any) {
	uv.WithLevel("info", args...)
}

// Warn sends a message with the "warn" log level.
func (uv *Logger) Warn(args ...any) {
	uv.WithLevel("warn", args...)
}

// Error sends a message with the "error" log level.
func (uv *Logger) Error(args ...any) {
	uv.WithLevel("error", args...)
}

// Fatal sends a message with the "critical" log level.
func (uv *Logger) Fatal(args ...any) {
	uv.WithLevel("critical", args...)
	uv.ExitFunction(args...)
}

// Critical sends a message with the "critical" log level.
func (uv *Logger) Critical(args ...any) {
	uv.WithLevel("critical", args...)
	uv.ExitFunction(args...)
}
