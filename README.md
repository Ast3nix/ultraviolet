<hr>

<img alt="Ultraviolet — the 15th standard (xckd #927!)" src="assets/ultraviolet-title.png" width="100%" />

<hr><br>
<div style="font-size: x-large">

**Ultraviolet** is a simple, non-structured logging library for Go. Documentation is available on [pkg.go.dev](https://pkg.go.dev/codeberg.org/Ast3nix/ultraviolet)
Ultraviolet should not be considered stable or fit for production environments.

<br>
</div>

<hr><br>

<img alt="Alacritty screenshot showcasing Ultraviolet" src="assets/screenshot.png" width="100%" /><br>

```go
package main

import (
	"math"

	"codeberg.org/Ast3nix/ultraviolet"
)

var uv = ultraviolet.New()

func main() {
	uv.Info(math.Pi, -1, 0, 1, uint32(2), true, false, []string{"hello to whoever is reading this"})

	uv.Subscope("level1").Critical("a")
	uv.Subscope("level1").Subscope("error").Error("a")
	uv.Subscope("level1").Subscope("warn").Warn("a")
	uv.Subscope("level1").Subscope("info").Info("a")
	uv.Subscope("level1").Subscope("debug").Debug("a")
}
```
